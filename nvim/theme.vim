" Nightfox Nord
" ---------------
colorscheme nightfox
lua << EOF
local nightfox = require('nightfox')
nightfox.setup {
  fox = "nordfox",
}
nightfox.load()
EOF

" Indent
lua << EOF
require("indent_blankline").setup {
	char = "┊",
	show_first_indent_level = false,
	buftype_exclude = {"terminal"},
}
EOF

" Lightline
" ---------------
let g:lightline = {'colorscheme': 'nightfox',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ], [ 'readonly', 'relativepath', 'modified' ] ],
      \ }
\ }

" Disable -- MODE --
set noshowmode

" tabulations
set tabstop=4
set shiftwidth=4
