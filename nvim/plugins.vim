call plug#begin('~/.local/share/nvim/plugged')

" Better syntax support
" Plug 'sheerun/vim-polyglot'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Nvim Tree
Plug 'kyazdani42/nvim-web-devicons' " for file icons
Plug 'kyazdani42/nvim-tree.lua'

Plug 'lukas-reineke/indent-blankline.nvim'

" Lightline status bar
Plug 'itchyny/lightline.vim'

" Nord Theme
" Plug 'arcticicestudio/nord-vim'
Plug 'EdenEast/nightfox.nvim'

call plug#end()
