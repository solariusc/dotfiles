" General settings
set number					" line numbers
set wrap
set cursorline			" cursor line highlight
set scrolloff=5
set mouse=a

" Bepo key mapping
source $HOME/.config/nvim/bepo.vim

" Plugins
source $HOME/.config/nvim/plugins.vim

" Theme
source $HOME/.config/nvim/theme.vim

" Nvim tree
source $HOME/.config/nvim/tree.vim

" Navigation de page simplifiée
noremap <BS> H
noremap <Space> L
noremap <Return> zz

" Session management
nnoremap <Leader>ms :mksession! ~/.local/share/nvim/sessions/
nnoremap <Leader>os :source ~/.local/share/nvim/sessions/
nnoremap <Leader>ds :!rm ~/.local/share/nvim/sessions/

" Spits
nnoremap <Leader>/ :vsp<CR>
nnoremap <Leader>- :sp<CR>

" Ctrl+S to save
noremap <silent> <C-S> :update<CR>
vnoremap <silent> <C-S> <Esc>:update<CR>
inoremap <silent> <C-S> <Esc>:update<CR>

" Ctrl+Q to quit
noremap <silent> <C-Q> :qa<CR>
noremap <silent> q :qa<CR>

" Ctrl+E to exit
noremap <silent> <C-E> :exit<CR>

" Reload config
noremap <Leader>c :so $MYVIMRC<CR>

